# Approach 1: Using attr_accessor and Setting Attributes After Object Initialization

# class Book
#   attr_accessor :title,:author, :pages
# end

# book1=Book.new()
# book1.title= 'nami'
# book1.author='king'
# puts book1.title


# book2=Book.new()
# book2.title= 'zoro'
# book2.author='oda'

# puts book2.title



# Approach 2: Using attr_accessor and Initializing Attributes in the initialize Method

# class Book
#   attr_accessor :title,:author,:pages
#   def initialize(title,author,pages)
#     @title=title
#     @author=author
#     @pages=pages
#   end

#   def has_honors
#     if @pages>300
#       return true
#     else
#       return false
#     end
#   end

# end

# book1=Book.new('One Piece',"Oda",10000)
# puts book1.title
# puts book1.has_honors



begin
  # by using we question class we are
  #   defining the answers setted in an array of the question and answers
end

# class Question
#   attr_accessor :prompt, :answer
#   def initialize(prompt, answer)
#     @prompt=prompt
#     @answer=answer
#   end
# end

# p1="What color are apples?\n(a)red\n (b)Purple\n(c)Orange"
# p2="What color are baanans?\n(a)pink\n (b)Purple\n(c)yellow"
# p3="What color are pears?\n(a)\yellow\n (b)green\n(c)Orange"

# questions=[
#   Question.new(p1,"a"),
#   Question.new(p2,"c"),
#   Question.new(p3,"b")
# ]

# def run_test(questions)
#   answer=""
#   score=0
#   for question in questions
#     puts question.prompt
#     answer=gets.chomp()
#     if answer==question.answer
#       score+=1
#     end
#   end
#   print "You got ",score,"/",questions.length(),"\n"
# end

# run_test(questions)



begin
  # Inheritence
end
# class Chef
#   def make_chicken
#     puts "The chef makes chicken"
#   end
#   def make_salad
#     puts "The chef makes salad"
#   end
#   def make_special_dish
#     puts "The chef makes bbq ribs"
#   end
# end

# class ItalianChef < Chef
#   def make_pasta
#     puts "The chef makes pasta"
#   end
# end

# chef=ItalianChef.new()
# puts chef.make_chicken

begin
  
end
